import React, { useState } from 'react';
import {
  required,
  match,
  multiple,
  hasError,
  validateMultiple,
  sameAsOtherFiled,
  anyErrors
} from './formValidation';
const Step = ({
  currentStep,
  handleChangeStep,
  title,
  handleSaveStepObject,
}) => {
  const [validationErrors, setValidationErrors] = useState({});
  const [stepOneObject, setStepOneObject] = useState({});
  const { email = '', number = '', date = '', time = '', name = '' } = stepOneObject;

  const validators = {
    email: multiple(
      required('Email is required'),
      match(/^([\w-_]+(?:\.[\w-_]+)*)@((?:[a-z0-9]+(?:-[a-zA-Z0-9]+)*)+\.[a-z]{2,6})$/i,
        'Should be name@domain'),
      //TODO 可以根据需求添加自定义的验证
    ),
    number: multiple(
      required('Number is required'),
      match(
        /^[0-9+()\- ]*$/,
        'Only numbers, spaces and these symbols are allowed: ( ) + -'
        //TODO 可以根据需求添加自定义的验证
      )
    ),
    date: required('Date is required'),
    time: required('Time is required'),
    name: multiple(
      required('Name is required'),
      sameAsOtherFiled(email, 'Cannot same as email')
    )
  };

  const validateSingleField = (fieldName, fieldValue) => {
    const result = validateMultiple(validators, {
      [fieldName]: fieldValue
    });
    setValidationErrors({ ...validationErrors, ...result });
  };

  const handleChange = ({ target }) => {
    setStepOneObject(stepOneObject => ({
      ...stepOneObject,
      [target.name]: target.value
    }));
    if (hasError(validationErrors, target.name)) {
      validateSingleField(target.name, target.value);
    }
  };
  const handleBlur = ({ target }) =>
    validateSingleField(target.name, target.value);

  const handleNext = (e) => {
    e.preventDefault();
    const validationResult = validateMultiple(validators, { email, number, date, time, name });
    if (!anyErrors(validationResult) && currentStep.step < 4) {
      handleChangeStep(++currentStep.step);
      handleSaveStepObject({ [currentStep.key]: stepOneObject })
    } else {
      setValidationErrors(validationResult);
    }
  };

  return (
    <main>
      <h1>Current step is {title}</h1>
      <div>
        <label htmlFor="name">Name: </label>
        <input type="text" name="name" id="name"
          onChange={handleChange}
          onBlur={handleBlur}
          value={name} />
        {validationErrors['name'] && <p>{validationErrors['name']}</p>}
      </div>
      <div>
        <label htmlFor="email">Email: </label>
        <input type="email" name="email" id="email"
          onChange={handleChange}
          onBlur={handleBlur}
          value={email} />
        {validationErrors['email'] && <p>{validationErrors['email']}</p>}
      </div>
      <div>
        <label htmlFor="number">Number: </label>
        <input type="number" name="number" id="number"
          onChange={handleChange}
          onBlur={handleBlur}
          value={number} />
        {validationErrors['number'] && <p>{validationErrors['number']}</p>}
      </div>
      <div>
        <label htmlFor="date">Date: </label>
        <input type="date" name="date" id="date"
          onChange={handleChange}
          onBlur={handleBlur}
          value={date} />
        {validationErrors['date'] && <p>{validationErrors['date']}</p>}
      </div>
      <div>
        <label htmlFor="time">Time: </label>
        <input type="time" name="time" id="time"
          onChange={handleChange}
          onBlur={handleBlur}
          value={time} />
        {validationErrors['time'] && <p>{validationErrors['time']}</p>}
      </div>
      <button type="button" onClick={handleNext}>Next</button>
    </main>
  )
};

export default Step;