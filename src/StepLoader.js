import React, { useState } from 'react';
import Step from "./Step";

const StepLoader = () => {
    const steps = [
        { key: 'step1', title: 'step1', step: 0 },
        { key: 'step2', title: 'step2', step: 1 },
        { key: 'step3', title: 'step3', step: 2 }
    ];

    const [currentStep, changeStep] = useState({ key: 'step1', title: 'step1', step: 0 });
    const [allStepObject, setAllStepObject] = useState({});

    const handleChangeStep = (nextStep) => {
        console.log('nextStep', nextStep);
        console.log('allStepObject', allStepObject);
        const __currentStep = steps
            .find(({ step }) => step === nextStep) || {};
        changeStep(__currentStep);
    };
    const handleSaveStepObject = (currentStepObject) => {
        setAllStepObject({ ...allStepObject, ...currentStepObject })
    }

    const stepProps = {
        currentStep,
        handleChangeStep,
        allStepObject,
        setAllStepObject,
        handleSaveStepObject,
    };
    const renderResult = (results) => Object
        .keys(results)
        .map((key) => (<pre key={key}>{key}: {JSON.stringify(results[key], null, 3)}</pre>))
    return (
        <>
            {renderResult(allStepObject)}
            <Step {...currentStep} {...stepProps} />
        </>

    )
}

export default StepLoader
