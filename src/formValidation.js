export const anyErrors = errors =>
    Object.values(errors).some(error => error !== undefined);

export const required = description => value =>
    !value || value.trim() === '' ? description : undefined;

export const match = (re, description) => value =>
    !value.match(re) ? description : undefined;

export const hasError = (validationErrors, fieldName) =>
    validationErrors[fieldName] !== undefined;

export const sameAsOtherFiled = (current, description) => value => {
    return current === value ? description : undefined;
}

export const validateMultiple = (validators, fields) =>
    Object.entries(fields).reduce(
        (result, [name, value]) => ({
            ...result,
            [name]: validators[name](value)
        }),
        {}
    );

export const multiple = (...validators) => value =>
    validators.reduce(
        (result, validator) => result || validator(value),
        undefined
    );
